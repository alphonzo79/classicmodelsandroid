package com.lab131.classicmodelsandroid;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lab131.classicmodelsandroid.domain.Customer;
import com.lab131.classicmodelsandroid.domain.Root;

import org.springframework.hateoas.hal.Jackson2HalModule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CustomerListActivity extends AppCompatActivity {

	private ArrayList<Customer> listItems=new ArrayList<Customer>();
	private ListView customerListView;
	private Button addMoreButton;
	private Button clearListButton;
	private Button createCustomerButton;
	private ArrayAdapter<Customer> listItemsAdapter = null;
//	ArrayAdapter<String> arrayAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_customer_list);
		setupUI();
		loadUsersOnStartup();
	}

	private static final class HttpRequestTask extends AsyncTask<Void, Void, ArrayList> {

		private ArrayList<Customer> innerListItems;
		private ArrayAdapter<Customer> arrayAdapter;
		public HttpRequestTask(ArrayList<Customer> outterListItems, ArrayAdapter<Customer> outterArrayAdapter) {
			this.innerListItems = outterListItems;
			this.arrayAdapter = outterArrayAdapter;
		}

		@Override
		protected ArrayList<Customer> doInBackground(Void... voids) {
			try {
				String url = "http://classicmodels-lab131.rhcloud.com/customers/";
				OkHttpClient client = new OkHttpClient();

				Request request = new Request.Builder()
						.url("http://classicmodels-lab131.rhcloud.com/customers")
						.build();
				Response response = client.newCall(request).execute();
				if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.registerModule(new Jackson2HalModule());

				String dyncJson_1 = response.body().string();
				Root root = mapper.readValue(dyncJson_1, Root.class);

				System.out.print("I'm gonna tear up the dance floor (using a different mapper method)");
				List<Customer> customersDynamic2 = mapper.convertValue(root.getEmbeddedResources().get("customers"), new TypeReference<List<Customer>>(){});
				Customer customer2 = customersDynamic2.get(0);
				System.out.println(customer2.getCustomerName());
				innerListItems.clear();
				for(Customer customer : customersDynamic2) {
					innerListItems.add(customer);
				}
				return innerListItems;

			} catch (Exception e) {
				Log.e("CustomerListActivity", e.getMessage(), e);
			}

			return null;
		}

		@Override
		protected void onPostExecute(ArrayList innerListItems) {
			super.onPostExecute(innerListItems);
			if(innerListItems==null) {
				return;
			}
			this.arrayAdapter.notifyDataSetChanged();
		}
	}

	private void setupUI() {
		listItemsAdapter =  new ArrayAdapter<Customer>(this, android.R.layout.simple_list_item_1, listItems);
		addMoreButton = (Button) findViewById(R.id.buttonAddMore);
		createCustomerButton = (Button) findViewById(R.id.buttonCreateNewCustomer);
		clearListButton = (Button) findViewById(R.id.buttonClearList);
		customerListView = (ListView) findViewById(R.id.customerListView);
		customerListView.setAdapter(listItemsAdapter);

		customerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String item = ((TextView)view).getText().toString();
				Customer customer = listItems.get(position);
				Intent intent = new Intent(getApplicationContext(), CustomerDetailsActivity.class);
				intent.putExtra("customerNumber", customer.getCustomerNumber());
//				Toast.makeText(getBaseContext(), item, Toast.LENGTH_LONG).show();
				startActivity(intent);
			}
		});

		addMoreButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseAdapter)customerListView.getAdapter()).notifyDataSetChanged();

				new HttpRequestTask(listItems, listItemsAdapter).execute();
			}
		});
		createCustomerButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), NewCustomerActivity.class);

				startActivity(intent);
			}
		});
		clearListButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				listItemsAdapter.clear();
			}
		});
	}

	private void loadUsersOnStartup() {
		((BaseAdapter)customerListView.getAdapter()).notifyDataSetChanged();
		new HttpRequestTask(listItems, listItemsAdapter).execute();
	}

}
