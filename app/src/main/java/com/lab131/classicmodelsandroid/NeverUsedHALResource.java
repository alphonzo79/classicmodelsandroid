package com.lab131.classicmodelsandroid;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.hateoas.ResourceSupport;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chadj on 5/30/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NeverUsedHALResource extends ResourceSupport {
	private final Map<String, ResourceSupport> embedded = new HashMap<String, ResourceSupport>();

	@JsonProperty("_embedded")
	public Map<String, ResourceSupport> getEmbeddedResources() {
		return embedded;
	}

	public void embedResource(String relationship, ResourceSupport resource) {

		embedded.put(relationship, resource);
	}
}
