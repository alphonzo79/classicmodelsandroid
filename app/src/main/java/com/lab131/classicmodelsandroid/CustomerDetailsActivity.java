package com.lab131.classicmodelsandroid;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lab131.classicmodelsandroid.domain.Customer;
import com.lab131.classicmodelsandroid.domain.Root;

import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CustomerDetailsActivity extends AppCompatActivity implements AsyncResponse {
	TextView customerNameTextView;
	TextView customerNumberTextView;
	TextView countryTextView;
	TextView cityTextView;
	TextView stateTextView;
	TextView postalCodeTextView;
	TextView customerContactLastNameTextView;
	TextView customerContactFirstNameTextView;
	TextView cusomterCreditRatingTextView;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setupUI();
	}

	private class HttpRequestTask extends AsyncTask<Void, Void, Customer> {

		private Customer customer;
		private final TextView custNameTextView;
		private final String customerNumber;
		private final TextView customerNumberTextView;
		private TextView countryTextView;

		public HttpRequestTask(Customer customer, TextView custNameTextView, String customerNumber,
							   TextView customerNumberTextView, TextView countryTextView) {
			this.customer = customer;
			this.custNameTextView = custNameTextView;
			this.customerNumber = customerNumber;
			this.customerNumberTextView = customerNumberTextView;
			this.countryTextView = countryTextView;
		}

		@Override
		protected Customer doInBackground(Void... voids) {
			try {
				String url = "http://classicmodels-lab131.rhcloud.com/customers/"+this.customerNumber;

				/*** B Section Start **/
//				RestTemplate restTemplate = restTemplate();
//
//				ParameterizedTypeReference<PagedResources<Customer>> responseTypeRef = new ParameterizedTypeReference<PagedResources<Customer>>() {
//				};
//				ResponseEntity<PagedResources<Customer>> responseEntity = restTemplate.exchange(url, HttpMethod.GET,
//						(HttpEntity<Customer>) null, responseTypeRef);
//
//				PagedResources<Customer> resources = responseEntity.getBody();
//				Collection<Customer> customers = resources.getContent();
//				List<Customer> userList = new ArrayList<Customer>(customers);

				/** B Section Ends **/


				/*** A Section start ***/
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.registerModule(new Jackson2HalModule());

				MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
				converter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/hal+json"));
				converter.setObjectMapper(mapper);

				RestTemplate template = new RestTemplate(Collections.<HttpMessageConverter<?>> singletonList(converter));
//				List<HttpMessageConverter<?>> converterList = new ArrayList<HttpMessageConverter<?>>();
//				converterList.add(converter);
//				RestTemplate template = new RestTemplate(converterList);
				this.customer = template.getForObject(url, Customer.class);

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						postalCodeTextView.setText(customer.getPostalCode());
						stateTextView.setText(customer.getState());
						cityTextView.setText(customer.getCity());
						customerContactFirstNameTextView.setText(customer.getContactFirstName());
						customerContactLastNameTextView.setText(customer.getContactLastName());
						cusomterCreditRatingTextView.setText("Credit Rating: "+customer.getCreditLimit());
					}
				});

				/*** A Section End ***/
				return customer;

			} catch (Exception e) {
				Log.e("CustomerDetailsActivity", e.getMessage(), e);
			}

			return null;
		}

		private RestTemplate restTemplate() {

			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.registerModule(new Jackson2HalModule());

			MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
			converter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/hal+json"));
			converter.setObjectMapper(mapper);

			List<HttpMessageConverter<?>> converterList = new ArrayList<HttpMessageConverter<?>>();
			converterList.add(converter);
			RestTemplate restTemplate = new RestTemplate(converterList);

			return restTemplate;
		}

		@Override
		protected void onPostExecute(Customer customer) {
			super.onPostExecute(customer);
			if(customer==null) {
				this.customerNumberTextView.setText(this.customerNumber);
				this.custNameTextView.setText("Not a customer");
				return;
			}
			this.custNameTextView.setText(customer.getCustomerName());
			String number = customer.getCustomerNumber()+"";
			this.customerNumberTextView.setText(number);
			this.countryTextView.setText(customer.getCountry());
		}
	}

	public void setupUI() {

		customerNameTextView = (TextView) findViewById(R.id.textViewCustomerName);
		customerNumberTextView = (TextView) findViewById(R.id.textViewCustomerNumber);
		countryTextView = (TextView) findViewById(R.id.textViewCountry);
		cityTextView = (TextView) findViewById(R.id.textViewCity);
		stateTextView = (TextView) findViewById(R.id.textViewState);
		postalCodeTextView = (TextView) findViewById(R.id.textViewPostalCode);
		customerContactLastNameTextView = (TextView) findViewById(R.id.textViewCustomerContactLastName);
		customerContactFirstNameTextView = (TextView) findViewById(R.id.textViewCustomerContactFirstName);
		cusomterCreditRatingTextView = (TextView) findViewById(R.id.textViewCreditRating);

		Bundle extras = getIntent().getExtras();
		if(extras!=null) {

			String customerNumber = extras.getInt("customerNumber") + "";

			Customer customer = new Customer();
			new HttpRequestTask(customer, customerNameTextView, customerNumber, customerNumberTextView, countryTextView).execute();
		}

	}

	@Override
	public void processFinish(Object output) {

	}
}
