package com.lab131.classicmodelsandroid.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chadj (dontpanic@bodybuilding.com) on 6/15/16
 *
 * @author chadj
 * @version 1.0.0
 * @since 6/15/16
 */
abstract class HALResource extends ResourceSupport {

	private final Map<String, Object> embedded = new HashMap<>();

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("_embedded")
	public Map<String, Object> getEmbeddedResources() {
		return embedded;
	}

	public void embedResource(String relationship, Object resource) {

		embedded.put(relationship, resource);
	}

}
