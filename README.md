# **ClassicModels Android App**

## **Description**
* Simply, this is a demo app that runs against a demo API that operates on demo data from the "ClassicModels" database. It's purpose was/is to learn and discover functionality in Android. 

## **Gotchas**
* Sometime if the webserver hasn't been contacted in a while it goes into a hibernate mode and takes a few minutes to startup. The base end point is http://classicmodels-lab131.rhcloud.com, to see the API, select the HAL browser. Or you can request that end point with an accept header of application/hal+json. The other link uses Handlebars templates to render data in HTML. Will link to this git repo sometime soon.

## **Requirements**
* AndroidStudio 2.1+
* JDK 8

## **Branches**
* I will create various branches or tags in the future that demonstrate progression and evolution of this app.

## **Bug Tracking/Feature Requests**
* Use https://bitbucket.org/chadunplugged/somali/issues?status=new&status=open or submit a pull request.
