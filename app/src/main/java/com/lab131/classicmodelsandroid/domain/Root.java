package com.lab131.classicmodelsandroid.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import java.util.Map;

/**
 * Created by chadj (dontpanic@bodybuilding.com) on 6/18/16
 *
 * @author chadj
 * @version 1.0.0
 * @since 6/18/16
 */
public class Root extends HALResource {

//	private Customers customers;

	public Root() {
	}

//	public Root(Customers customers) {
//		this.customers = customers;
//	}
//
//
//	public Customers getCustomers() {
//		return customers;
//	}
//
//	public void setCustomers(Customers customers) {
//		this.customers = customers;
//	}

	//	private Map<String, Object> embedded;
//
//	public Root() {
//	}
//
//	public Root(Map<String, Object> embedded) {
//		this.embedded = embedded;
//	}
//
//	@JsonInclude(JsonInclude.Include.NON_EMPTY)
//	@JsonProperty("_embedded")
//	public Map<String, Object> getEmbedded() {
//		return embedded;
//	}
//
//	public void setEmbedded(Map<String, Object> embedded) {
//		this.embedded = embedded;
//	}
}
